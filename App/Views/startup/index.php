<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <div class="row">
    <br>
    <div class="col-md-12">
        <a href="http://<?php echo APP_HOST; ?>/produto/cadastro" class="btn btn-success btn-sm">Adicionar</a>
        <hr>
    </div>
    <div class="col-md-12">
        <?php if($Sessao::retornaMensagem()){ ?>
            <div class="alert alert-warning" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $Sessao::retornaMensagem(); ?>
            </div>
        <?php } ?>

        <?php
            if(!count($viewVar['listaProdutos'])){
        ?>
            <div class="alert alert-info" role="alert">Nenhum produto encontrado</div>
        <?php
            } else {
        ?>
            
            <div class="table-responsive">
                <table class="table table-bordered table-hover status_info">
                    <tr>
                        <td class="info">Nome</td>
              
                        <td class="info">Descrição</td>
                        
                        <td class="info">Data de Início</td>
                        <td class="info">Data de Fim</td>
                        
                        <td class="info">Status</td>
                        <td class="info">Situação</td>
                        
                        <td class="info"></td>
                    </tr>
                    <?php
                        foreach($viewVar['listaProdutos'] as $produto) {
                    ?>
                    
                        <!--td class="status_info"-->
                            <tr>
                                <td><?php echo $produto->getNome(); ?></td>
                            
                                <!--    
                                <td>R$ <?php echo $produto->getPreco(); ?></td>
                                <td><?php echo $produto->getQuantidade(); ?></td>
                                <td><?php echo $produto->getDataCadastro()->format('d/m/Y'); ?></td>
                                -->    
                                
                                <td><?php echo $produto->getDescricao(); ?></td>
                                <td><?php echo $produto->getDataInicio(); ?></td>
                                <td><?php echo $produto->getDataFim(); ?></td>
                                
                                <td><?php echo $produto->getIdStatus_literal(); ?></td>
                                <td><?php echo $produto->getIdSituacao_literal(); ?></td>
                            
                                <td>
                                    <a href="http://<?php echo APP_HOST; ?>/produto/edicao/<?php echo $produto->getId(); ?>" class="btn btn-info btn-sm">Editar</a>
                                    <a href="http://<?php echo APP_HOST; ?>/produto/exclusao/<?php echo $produto->getId(); ?>" class="btn btn-danger btn-sm">Excluir</a>
                                </td>
                            </tr>
                        <!--/td-->
                        
                    <?php
                        }
                    ?>
                </table>
            </div>
        <?php
            }
        ?>
    </div>
</div>
</div>

<div class="container">
  <h2>Modal Example</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>



        <!-- Modal Edit !!!!!!!!!!!!!!!!!!!!!!! -->

        <!-- Begin: Modal Edit -->
        <div id="edit" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-width-meeta">
                <div class="modal-content">
                    <!-- Begin: Modal Header -->
                    <div class="modal-header bg-info bg-meeta">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">
                            <i class="mr10 fa fa-building"></i>
                            <span>Reclamação</span>
                        </h4>
                    </div>
                    <!-- End: Modal Header -->
                    <!-- Beign: Modal Body -->
                    <div class="modal-body modal-body-meeta">
                        <div class="panel panel-meeta mt-10">
                            <!-- Begin: Form Edit -->
                            <!-- EF 2016/Out - 08  Uploads multiplos-->
                            <form method="post" action="#" id="entity-form" enctype="multipart/form-data"  class="dropzone">

                                <!--input type="hidden" class="user_profile" name="user_profile" value=""-->

                                <!--EF 2016/Out - 05 -->
                                <input type="hidden" class="id_reclamacao" name="id_reclamacao" value="">

                                <div class="panel-body panel-body-meeta p10 pb10 tab-content pn br-n admin-form">
                                    <!-- Begin: Campos -->
                                    <div class="row">
                                        <!-- Seguradora -->
                                        <div class="col-xs-12 mb15">

                                            <!-- EF 2016/Out -03 Cliente Corportivo
                                            <label for="fk_seguradora" class="field-label text-muted mb10">Seguradora</label>
                                            -->
                                            <label for="fk_seguradora" class="field-label text-muted mb10">Cliente Corporativo</label>
                                            <label for="fk_seguradora" class="validar field select">
                                                <?php echo $fk_seguradora; ?>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!-- Data -->
                                        <div class="col-xs-4 mb15">
                                            <label for="data" class="field-label text-muted mb10">Data</label>
                                            <label for="data" class="validar field select">
                                                <!-- EF Dez/2016 -->
                                           
                                                <!--input type='text'id='data' name='data' class="form-control gui-input br-light light" placeholder=""/-->                                                         
                                                <input value="<?php echo date("d-m-Y H:i:s");?>" type='text'id='data' name='data' class="form-control gui-input br-light light" placeholder="" readonly=""/>
                         
                                                <!-- EF Dez/2016 -->
                                           	    <!--input value="" type='text'id='data' name='data' class="form-control gui-input br-light light" placeholder="" readonly=""/-->
                                                   <!--script type="text/javascript">
                         
                                                           //  console.log("editar ");
//                                                             var teste = ("#edit #id").val();
//                                                             console.log(teste);
                                                              
                                                             jQuery(function() {
                                                                
                                                                 //if($("#edit #id").val() == "") {
                                                                
                                                                      //console.log("novo ");
                                                                    
                                                                      var d2 = function(n) { return n>9?n:"0"+n;  };
                                                                      setInterval(function() {
                                                                        var d = new Date();
                                                                        var dd = d2(d.getDate());
                                                                        var mm = d2(1+d.getMonth());
                                                                        var yyyy = d.getFullYear();
                                                                        var hh = d2(d.getHours());
                                                                        var mi = d2(d.getMinutes());
                                                                        var ss = d2(d.getSeconds());        
                                                                        $("#edit #data").val( dd + "-" + mm + "-" + yyyy + " " + hh + ":" + mi + ":" + ss );
                                                                    }, 200);
                                                                //}
                                                             });
                                                        
                                                    </script-->                            
                               
                                            </label>
                                        </div>
                                        <!-- Interna/Externa -->
                                        <div class="col-xs-4 mb20">
                                            <label for="interna_externa" class="field-label text-muted mb10">Interna/Externa</label>
                                            <div class="mt15">
                                                <label class="radio-inline mr10">
                                                    <input type="radio" name="interna_externa" id="interna" value='I'>Interna
                                                </label>
                                                <label class="radio-inline mr10">
                                                    <input type="radio" name="interna_externa" id="externa" value="E">Externa
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <!-- Enviado Por -->
                                        <div class="col-xs-6 mb20">
                                            <label for class="field-label text-muted mb10">Enviado Por</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-cog c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="enviado_por" id="enviado_por" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                        <!-- Nome Reclamante -->
                                        <div class="col-xs-6 mb20">
                                            <label for class="field-label text-muted mb10">Nome do cliente (Reclamante)</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="nome_reclamante" id="nome_reclamante" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                        <!-- Genero -->
                                        <div class="col-xs-4 mb20">
                                            <label for="genero" class="field-label text-muted mb10">Genero</label>
                                            <label for="genero" class="validar field select">
                                                <select name="genero" id='genero'>
                                                    <option value="">Selecione ...</option>
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Feminino</option>
                                                </select>
                                            </label>
                                        </div>
                                        <!-- Interna/Externa -->
                                        <div class="col-xs-4 mb20">
                                            <label for="fisica_juridica" class="field-label text-muted mb10">Pessoa Fisica/Juridica</label>
                                            <div class="mt15">
                                                <label class="radio-inline mr10">
                                                    <input type="radio" name="fisica_juridica" id="fisica" value="F">Fisica
                                                </label>
                                                <label class="radio-inline mr10">
                                                    <input type="radio" name="fisica_juridica" id="juridica" value="J">Juridica
                                                </label>
                                            </div>
                                        </div>
                                        <!-- CPF CNPJ -->
                                        <div class="col-xs-4 mb20">
                                            <label for class="field-label text-muted mb10">CPF/CNPJ</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-cog c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="cpf_cnpj" id="cpf_cnpj" class="form-control gui-input br-light light" onkeypress='mascaraMutuario(this, cpfCnpj)' onblur='clearTimeout()' maxlength="18">
                                                </span>
                                            </div>
                                        </div>
                                        <!-- Data de Atendimento -->
                                        <div class="col-xs-4 mb15">
                                            <label for="data_atendimento" class="field-label text-muted mb10">Data de Atendimento</label>
                                            <label for="data_atendimento" class="validar field select">
                                                <input type='text'id='data_atendimento' name='data_atendimento' class="form-control gui-input br-light light" placeholder=""/>
                                            </label>
                                        </div>
                                        <!-- SISE -->
                                        <div class="col-xs-6 mb20">
                                            <label for class="field-label text-muted mb10">SISE</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-cog c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="sise" id="sise" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <!-- Nome do Produto -->
                                        <div class="col-xs-8 mb15">
                                            <label for="fk_nome_produto" class="field-label text-muted mb10">Nome do Produto</label>
                                            <label for="fk_nome_produto" class="validar field select">
                                                <?php echo $fk_nome_produto; ?>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- Servico -->
                                        <div class="col-xs-6 mb15">
                                            <label for="fk_servico" class="field-label text-muted mb10">Serviço</label>
                                            <label for="fk_servico" class="validar field select">
                                                <?php echo $fk_servico; ?>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- Sub_servico -->
                                        <div class="col-xs-6 mb15">
                                            <label for="fk_sub_servico" class="field-label text-muted mb10">Sub Serviço</label>
                                            <label for="fk_sub_servico" class="validar field select fk_sub_servico">
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <script type="text/javascript">
                                            //Carrega o Dropdown de sub_servico de acordo com a origem selecionada
                                            $(function () {
                                                $("#fk_servico").change(function () {
                                                    var serv = $('#fk_servico').val();

                                                    $(".validar.field.select.fk_sub_servico").empty();

                                                    $.ajax({
                                                        url: base_url('reclamacao/get_subServico/' + serv),
                                                        type: 'post',
                                                        cache: false,
                                                        contentType: false,
                                                        processData: false,
                                                        dataType: 'json',
                                                        data: serv,
                                                        success: function (data) {
                                                            $(".validar.field.select.fk_sub_servico").append(data);
                                                        },
                                                        error: function (jqXHR) {
                                                            //bootbox.alert(jqXHR.responseText);
                                                        }
                                                    });

                                                });
                                            });
                                        </script>

                                        <!-- UF -->
                                        <div class="col-xs-2 mb15">
                                            <label for="uf" class="field-label text-muted mb10">UF</label>
                                            <label for="uf" class="validar field select">
                                                <select name="uf" id="uf">
                                                    <option value="">Selecione ...</option>
                                                    <option value="AC">AC</option>
                                                    <option value="AL">AL</option>
                                                    <option value="AP">AP</option>
                                                    <option value="AM">AM</option>
                                                    <option value="BA">BA</option>
                                                    <option value="CE">CE</option>
                                                    <option value="DF">DF</option>
                                                    <option value="ES">ES</option>
                                                    <option value="GO">GO</option>
                                                    <option value="MA">MA</option>
                                                    <option value="MT">MT</option>
                                                    <option value="MS">MS</option>
                                                    <option value="MG">MG</option>
                                                    <option value="PA">PA</option>
                                                    <option value="PB">PB</option>
                                                    <option value="PR">PR</option>
                                                    <option value="PE">PE</option>
                                                    <option value="PI">PI</option>
                                                    <option value="RJ">RJ</option>
                                                    <option value="RN">RN</option>
                                                    <option value="RS">RS</option>
                                                    <option value="RO">RO</option>
                                                    <option value="RR">RR</option>
                                                    <option value="SC">SC</option>
                                                    <option value="SP">SP</option>
                                                    <option value="TO">TO</option>
                                                </select>
                                            </label>
                                        </div>
                                        <!-- Cidade -->
                                        <div class="col-xs-10 mb20">
                                            <label for="cidade" class="field-label text-muted mb10">Cidade</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="cidade" id="cidade" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <!-- Origem do Problema -->
                                        <div class="col-xs-6 mb15">
                                            <label for="fk_origem_problema" class="field-label text-muted mb10">Origem do Problema</label>
                                            <label for="fk_origem_problema" class="validar field select">
                                                <?php echo $fk_origem_problema; ?>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>

                                        <script type="text/javascript">
                                            //Carrega o Dropdown de motivo_reclamação de acordo com a origem selecionada
                                            $(function () {
                                                $("#fk_origem_problema").change(function () {
                                                    var serv = $('#fk_origem_problema').val();

                                                    $(".validar.field.select.fk_motivo_reclamacao").empty();

                                                    $.ajax({
                                                        url: base_url('reclamacao/get_motivo/' + serv),
                                                        type: 'post',
                                                        cache: false,
                                                        contentType: false,
                                                        processData: false,
                                                        dataType: 'json',
                                                        data: serv,
                                                        success: function (data) {
                                                            $(".validar.field.select.fk_motivo_reclamacao").append(data);
                                                        },
                                                        error: function (jqXHR) {
                                                            // bootbox.alert(jqXHR.responseText);
                                                        }
                                                    });

                                                });
                                            });
                                        </script>

                                        <!-- Motivo da Reclamacao -->
                                        <div class="col-xs-6 mb15">
                                            <label for="fk_motivo_reclamacao" class="field-label text-muted mb10">Motivo da Reclamação</label>
                                            <label for="fk_motivo_reclamacao" class="validar field select fk_motivo_reclamacao">
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>


                                        <!-- Base Prestador -->
                                        <div class="col-xs-6 mb15">
                                            <label for="fk_base_prestador" class="field-label text-muted mb10">Base Prestador</label>
                                            <label for="fk_base_prestador" class="validar field select">
                                                <?php echo $fk_base_prestador; ?>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- Nome do Agente -->
                                        <div class="col-xs-12 mb20">
                                            <label for="nome_agente" class="field-label text-muted mb10">Nome do Agente</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user  c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="nome_agente" id="nome_agente" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <!-- Enviado ao Departamento em: -->
                                        <div class="col-xs-4 mb15">
                                            <label for="enviado_dpto_em" class="field-label text-muted mb10">Enviado ao Departamento em:</label>
                                            <label for="enviado_dpto_em" class="validar field select">
                                                <input type='text'id='enviado_dpto_em' name='enviado_dpto_em' class="form-control gui-input br-light light" placeholder=""/>
                                            </label>
                                        </div>
                                        <!-- Resposta do Departamento em: -->
                                        <div class="col-xs-4 mb15">
                                            <label for="resposta_dpto_em" class="field-label text-muted mb10">Resposta do Departamento em:</label>
                                            <label for="resposta_dpto_em" class="validar field select">
                                                <input type='text'id='resposta_dpto_em' name='resposta_dpto_em' class="form-control gui-input br-light light" placeholder=""/>
                                            </label>
                                        </div>
                                        <!-- Responsavel -->
                                        <div class="col-xs-12 mb20">
                                            <label for="responsavel" class="field-label text-muted mb10">Responsavel</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="responsavel" id="responsavel" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                        <!-- Ações do Departamento -->
                                        <div class="col-xs-12 mb20">
                                            <label for="acoes_dpto" class="field-label text-muted mb10">Ações do Departamento</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-pencil c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="acoes_dpto" id="acoes_dpto" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                        <!-- Comentários|Reclamações|Elogios -->
                                        <div class="col-xs-12 mb20">
                                            <label for="comentarios" class="field-label text-muted mb10">Comentários|Reclamações|Elogios</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-comment c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="comentarios" id="comentarios" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                        <!-- Parecer de Qualidade -->
                                        <div class="col-xs-12 mb20">
                                            <label for="parecer" class="field-label text-muted mb10">Parecer de Qualidade</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-pencil c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="parecer" id="parecer" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <!-- Recebido em: -->
                                        <div class="col-xs-4 mb15">
                                            <label for="recebido_em" class="field-label text-muted mb10">Recebido em:</label>
                                            <label for="recebido_em" class="validar field select">
                                                <input type='text'id='recebido_em' name='recebido_em' class="form-control gui-input br-light light" placeholder=""/>
                                            </label>
                                        </div>
                                        <!-- Respondido em: -->
                                        <div class="col-xs-4 mb15">
                                            <label for="respondido_em" class="field-label text-muted mb10">Respondido em:</label>
                                            <label for="respondido_em" class="validar field select">
                                                <input type='text'id='respondido_em' name='respondido_em' class="form-control gui-input br-light light" placeholder=""/>
                                            </label>
                                        </div>
                                        <!-- Tempo Resposta -->
                                        <div class="col-xs-4 mb20">
                                            <label for="tempo_resposta" class="field-label text-muted mb10">Tempo Resposta</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-clock-o c-gray"></i>
                                                </span>
                                                <span class="validar">
                                                    <input type="text" name="tempo_resposta" id="tempo_resposta" class="form-control gui-input br-light light" placeholder="" readonly aria-invalid="false">
                                                </span>
                                            </div>
                                        </div>


                                        <script type="text/javascript">
                                            $(function () {

                                                //EF 2016/Out - 04
                                                //$('#tempo_resposta').click(function () {

                                                $('#respondido_em').focusout(function () {
                                                    var start_actual_time = $('#recebido_em').val();
                                                    var end_actual_time = $('#respondido_em').val();
                                                    $('#tempo_resposta').val(null);

                                                    var formData = {'start': start_actual_time, 'end': end_actual_time};

                                                    if (start_actual_time != '' && end_actual_time != '') {
                                                        $.ajax({
                                                            url: base_url('reclamacao/tempo_resposta/'),
                                                            type: 'POST',
                                                            data: formData,
                                                            success: function (data) {
                                                                $('#tempo_resposta').val(data);
                                                            },
                                                            error: function (jqXHR) {
                                                                bootbox.alert(jqXHR.responseText);
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        </script>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <!-- Houve Custo? -->
                                        <!-- EF 2016/Out - 03 -->
                                        <div class="col-xs-4 mb20">
                                            <label for="houve_custo" class="field-label text-muted mb10">Houve Custo?</label>
                                            <label for="houve_custo" class="validar field select">
                                                <select name="houve_custo" class='houve_custo'>
                                                    <option value="">Selecione ...</option>
                                                    <option value="S">Sim</option>
                                                    <option value="N">Não</option>
                                                </select>
                                            </label>
                                        </div>

                                        <!-- Custo -->
                                       	<!-- EF Set/2016  - div-->
                                        <div id="divCusto">
                                            <div class="col-xs-4 mb20 custo">
                                                <label for="custo" class="field-label text-muted mb10">Custo</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-money c-gray"></i>
                                                    </span>
                                                    <span class="validar">
                                                        <input type="text" name="custo" id ="custo" class="custo form-control gui-input br-light light" placeholder="">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <!-- EF Set/2016  - div-->

                                    <div class="row">         
                                        <!-- EF Set/2016  - Houve Reembolso?-->

                                        <!-- EF 2016/Out - 03 -->
                                        <div id="divHouveReembolso">
                                            <div class="col-xs-4 mb20">
                                                <label for="houve_reembolso" class="field-label text-muted mb10">Houve Reembolso?</label>
                                                <label for="houve_reembolso" class="validar field select">
                                                    <select name="houve_reembolso" class='houve_reembolso'>
                                                        <option value="">Selecione ...</option>
                                                        <option value="S">Sim</option>
                                                        <option value="N">Não</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>




                                        <div id="divReembolso">
                                            <!-- EF Set/2016  - Reembolso?-->
                                            <div class="col-xs-4 mb20 reembolso">
                                                <label for="reembolso" class="field-label text-muted mb10">Reembolso</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-money c-gray"></i>
                                                    </span>
                                                    <span class="validar">
                                                        <input type="text" name="reembolso" id="reembolso" class="reembolso form-control gui-input br-light light" placeholder="">
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- EF Set/2016 FIM  - Reeembolso -->
                                        </div>




                                    </div>
                                    <!-- EF Set/2016 FIM  - div -->



                                    <!-- EF 2016/Out - 07  - Apresenta Anexo anterior, caso existente, ao Editar-->
                                    <hr class="anexo">  
                                    <div class='row anexo'>
                                        <div class="col-xs-20 mb40 reembolso">
                                            <label for="anexo" class="field-label text-muted mb10 anexo">Anexo</label>
                                            <div id="anexo" class="anexo"></div>
                                        </div>
                                    </div>

                                    <!-- Anexo -->
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-20 mb40">
                                            <!-- EF 2016/Out - 08 -- Uploads de multiplos arquivos -->
                                            <input name="file[]" type="file" multiple />
                                        </div>
                                    </div>

                                    <!--Email -->
<!--                                    <div class="modal-footer">
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 mb20">
                                            <label for="fk_email" class="field-label text-muted mb10">Email</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope c-gray"></i>
                                                </span>
                                                <label for="fk_email" class="validar field select">
                                                    <?php  echo $fk_email;  ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 mb20">
                                            <label for="cc_email" class="field-label text-muted mb10">CC</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-cc c-gray"></i>
                                                </span>
                                                <span>
                                                    <input type="text" name="cc_email" id="cc_email" class="form-control gui-input br-light light" placeholder="">
                                                </span>
                                            </div>
                                        </div>
                                    </div>-->
                                    <!-- End: Campos-->   

                            </form>
                            <!-- End: Form Edit -->
                        </div>
                    </div>                                
                    <!-- End: Modal Body-->

                    <!-- Begin: Modal Footer -->

                    <div class="modal-footer actions">
                        <div class="col-xs-3 col-xs-offset-1 pln">
                            <!-- EF 2016/Out elimina data-dismiss="modal"  Valida mas fecha e não salva -->
                            <!--button class="btn btn-warning btn-gradient btn-alt btn-block item-active andamento save_edit" data-dismiss="modal">Em Análise</button-->
                            <button class="btn btn-warning btn-gradient btn-alt btn-block item-active andamento save_edit">Em Análise</button>
                        </div>

                        <div class="col-xs-3 pln ">
                            <button class="btn btn-success btn-gradient btn-alt btn-block item-active save_edit">Procedente</button>
                        </div>

                        <div class="col-xs-3 pln ">
                            <button class="btn btn-danger btn-gradient btn-alt btn-block item-active improcedente save_edit" >Improcedente</button>
                        </div>

                    </div>

                    <!-- End: Modal Footer -->
                </div>
            </div>


        </div>

    </div>
    <!-- EF Out/2016 - 01 - FIM Modal de Edição -->


</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
$(function() {
	$('.table tr').each(function(i,e) {
		if ($('td:eq(4)', this).text() == 'Concluido') {
			$(this).css({backgroundColor: 'green'});
            
            //$('.table btn', this).prop('disabled', true);
            $('.btn', this).addClass('disabled');
            
		}
	});
});
</script>

















